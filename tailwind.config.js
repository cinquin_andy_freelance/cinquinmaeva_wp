const colors = require('tailwindcss/colors')

module.exports = {
  purge: {
      enabled:true,
      content:['./public/*.html'],
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    fontFamily: {
      display: ['Gilroy', 'sans-serif'],
      body: ['Graphik', 'sans-serif'],
      title:['Satisfy','sans-serif'],
      texte:['dita','sans-serif'],
    },
    filter: { // defaults to {}
      'none': 'none',
      'grayscale': 'grayscale(1)',
      'invert': 'invert(1)',
      'sepia': 'sepia(1)',
      'blur': 'blur(10px)',
      'brightness': 'brightness(0.1)',
      'custom': 'blur(3px) brightness(0.1)'
    },
    backdropFilter: { // defaults to {}
      'none': 'none',
      'blur': 'blur(10px)',
      'blurfooter': 'blur(30px)',
      'brightness': 'brightness(0.1)',
    },
    colors: { // defaults to {}
      maeva: {
        g50: '#EEEEEE',
        g100: '#E5E5E5',
        g200: '#e5e5e5',
        g300: '#D9D9D9',
        g400: '#B3B3B3',
        g500: '#737373',
        g600: '#333333',
        g700: '#2C2C2C',
        g800: '#191919',
        g900: '#131313',
        g1000: '#0A0A0A',
        g1100: '#010101',
        blue: '#255069',
        green: '#1F695B',
      },
      black: colors.black,
      white: colors.white,
      transparent: 'transparent',
      current: 'currentColor',
    },
    extend: {
      translate:{
        'nav':'-56px',
        'footerbtn':'-40px',
      },
      transitionDuration:{
        '2000':'2000ms',
        '3000':'3000ms',
        '4000':'4000ms',
        '5000':'5000ms',
        '6000':'6000ms',
        '7000':'7000ms',
      },
      scale:{
        '101':'1.01',
        '102':'1.02',
        '103':'1.03',
        '104':'1.04',

      },
      inset:{
        '-4.5':'-1.15rem',
      },
      spacing:{
        '20px':'20px',
        '30px':'30px',
        '40px':'40px',
        '56px':'56px',
        '100px':'100px',
        '200px':'200px',
        '300px':'300px',
        '400px':'400px',
        '500px':'500px',
        '600px':'600px',
        '700px':'700px',
        '800px':'800px',
        '900px':'900px',
        '1000px':'1000px',
      }
    }
  },
  variants: {
    filter: ['responsive'], // defaults to ['responsive']
    backdropFilter: ['responsive'], // defaults to ['responsive']
    extend: {
    },
  },
  plugins: [
    require('tailwindcss-filters'),
  ],
}
